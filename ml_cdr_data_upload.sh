DAYAGO=1

#!/bin/sh	
# ログ出力関数----------------------------------------------------------------------	
function log () {	
   # ログ出力先とログ名を設定。ログ名は自動で[シェル名].logとなる	
   LOG="./$(echo $0 | sed -e 's/.sh//g').log"	
	
   # ログ出力日時のフォーマットを yyyy/mm/dd hh:mm:ss に設定する	
   time=$(date '+%Y/%m/%d %T')	
	
   # 最初の引数の文字列をログ出力する	
   echo $time $1 >> $LOG	
	
   # 2番目の引数があればそれもログに出力する（エラーログで使用）	
   if [[ $2 != "" ]]; then	
       echo $2 >> $LOG	
   fi   	
}	
# ----------------------------------------------------------------------	
log "ml_cdr_data_upload.sh start"	
OUT_PATH="/root/ml_cdr/"	
OUTPUT="$OUT_PATH$CDR_TXT"	
TODAY_Y=`date +%Y`	
TODAY_m=`date +%m`	
TODAY_d=`date +%d`	
YESTERDAY_Y=`date +%Y -d "$DAYAGO day ago"`	
YESTERDAY_m=`date +%m -d "$DAYAGO day ago"`	
YESTERDAY_d=`date +%d -d "$DAYAGO day ago"`	
​	
SQL="select calldate || ',' || clid  || ',' || src  || ',' || dst  || ',' || dcontext  || ',' || channel  || ',' || dstchannel  || ',' || lastapp  || ',' || duration  || ',' || billsec  || ',' || disposition  || ',' || amaflags  || ',' || accountcode  || ',' || uniqueid  || ',' || userfield from (select calldate, clid, case dcontext WHEN 'agent_outgoing_outside_line' THEN substring(channel, strpos(channel, 'Local/') + 6, strpos(channel, '@') - strpos(channel, 'Local/') - 6) ELSE src END as src, dst, dcontext, channel, dstchannel, lastapp, lastdata, duration, billsec, disposition, amaflags, accountcode, uniqueid, userfield from cdr) AS cdr where calldate >= '${YESTERDAY_Y}-${YESTERDAY_m}-${YESTERDAY_d} 00:00:00' and calldate < '${TODAY_Y}-${TODAY_m}-${TODAY_d} 00:00:00' order by calldate asc;"	
source ./db_env1.txt	
USER="$db_user"	
PASS="$db_pass"	
HOST="$db_host"	
	
/usr/pgsql-11/bin/psql -U "$USER" -d "$PASS" -h "$HOST"-c "$SQL" -t | sed -e "s/^ //g" > sqlresult=$? $OUTPUT logresult=$?	
$  ; echo $?	
if [ sqlresult -ne 0 ]; then#直前のコマンドが正常ならば0、異常ならば1を返す	
log "ml_cdr_data_upload.sh SQL falled"
if [ logresult -ne 0 ]; then#直前のコマンドが正常ならば0、異常ならば1を返す
log "ml_cdr_data_upload.sh log falled"
	
source ./db_env2.txt	
USER="$db_user"	
PASS="$db_pass"	
HOST="$db_host"	
	
/usr/pgsql-11/bin/psql -U "$USER" -d "$PASS" -h "$HOST"-c "$SQL" -t | sed -e "s/^ //g" > sqlresult=$? $OUTPUT logresult=$?	
$  ; echo $?	
if [ sqlresult -ne 0 ]; then#直前のコマンドが正常ならば0、異常ならば1を返す	
log "ml_cdr_data_upload.sh SQL falled"
if [ logresult -ne 0 ]; then#直前のコマンドが正常ならば0、異常ならば1を返す
log "ml_cdr_data_upload.sh log falled"

source ./db_env3.txt	
USER="$db_user"	
PASS="$db_pass"	
HOST="$db_host"	
	
/usr/pgsql-11/bin/psql -U "$USER" -d "$PASS" -h "$HOST"-c "$SQL" -t | sed -e "s/^ //g" > sqlresult=$? $OUTPUT logresult=$?	
$  ; echo $?	
if [ sqlresult -ne 0 ]; then#直前のコマンドが正常ならば0、異常ならば1を返す	
log "ml_cdr_data_upload.sh SQL falled"
if [ logresult -ne 0 ]; then#直前のコマンドが正常ならば0、異常ならば1を返す
log "ml_cdr_data_upload.sh log falled"	
	
unix2dos $OUTPUT > /dev/null 2>&1	
	
